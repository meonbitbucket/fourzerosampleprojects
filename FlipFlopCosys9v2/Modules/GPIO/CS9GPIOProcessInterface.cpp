#include "CS9GPIOProcessInterface.h"

CCS9GPIOProcessInterface::ChannelPoller CCS9GPIOProcessInterface::channelPoller[4];
CIEC_WSTRING  CCS9GPIOProcessInterface::status_cache[4];
U32 CCS9GPIOProcessInterface::byCache[4];

CCS9GPIOProcessInterface::CCS9GPIOProcessInterface()
{	
	pinNum = -1;		
	byDMsk=0xFF;	
}

CCS9GPIOProcessInterface::~CCS9GPIOProcessInterface()
{
	deinitialise();
}

bool CCS9GPIOProcessInterface::initialise(CIEC_INT pin, CIEC_INT channel, 
										 EPinMode mode, CIEC_WSTRING &status)
{
	//todo: check pin and channel numbers
	this->dwChannel = channel;	
	this->pinMode = mode;	
	DWORD dwFkt;
	DWORD dwType = EA_PXM;
	DWORD dwSlot = 1;	
	if (this->pinMode == enDI)
		dwFkt = EA_DIN; 
	else
		dwFkt = EA_DOUT;
  
  if (pin >= 0 && pin <= 31)
  {
  	this->pinNum = pin;

	// =================== O P E N ======================	
	pRIdx = &RIdx;
	DWORD dwRet = EA_OpenRsrc( dwFkt, dwType, dwSlot, dwChannel, pRIdx);
	
	// =================== I N I T ======================
	if(dwRet != (0x0a000000|1) && dwRet) //by pass EA_COMMON_WARNING: 2+ sifb's may call init 2+ times
	{	
		status = "EA_OpenRsrc error";
		return false;
	}
	else
	{	
		if (this->pinMode == enDI)
			dwRet += EA_DIN_Init (this->RIdx);			
		
		else
			dwRet += EA_DOUT_Init (this->RIdx);			
			
		if (dwRet != (0x0a000000|1) && dwRet) //bypass EA_COMMON_WARNING
		{
			status = "EA_D**_Init error";
			return false;
		}
		
		//since init is complete start the thread for selected channel if not already alive
		ChannelPoller* currChannelPoller = &CCS9GPIOProcessInterface::channelPoller[this->dwChannel - 1];
			
		if (!currChannelPoller->isAlive())
		{
			currChannelPoller->pBasePI = this;
			currChannelPoller->start();
		}		
	}	
  }
  return true;
}

bool CCS9GPIOProcessInterface::deinitialise()
{
	//TODO: Release EAPI resources
	//Stop the thread for current channel
	ChannelPoller* currChannelPoller = &this->channelPoller[this->dwChannel];
	currChannelPoller->setAlive(false); //stop the polling thread
	EA_CloseRsrc(this->RIdx);			
  	return true;
}

bool CCS9GPIOProcessInterface::readPin(CIEC_BOOL &in, CIEC_WSTRING &status)
{	
	//pinNum is init at -1 and would remain -1 until a valid value was passed
	if(-1 != this->pinNum)
  	{				
		in = (CIEC_BOOL) (CCS9GPIOProcessInterface::byCache[this->dwChannel-1] & (1 << pinNum));
		status = CCS9GPIOProcessInterface::status_cache[this->dwChannel-1];
  	}	
  return true;
}

bool CCS9GPIOProcessInterface::writePin(const CIEC_BOOL &out, CIEC_WSTRING &status)
{
	//pinNum is init at -1 and would remain -1 until a valid value was passed
	if(-1 != this->pinNum)
	{		
		if (out)	
			CCS9GPIOProcessInterface::byCache[this->dwChannel-1] |= (1 << this->pinNum);
		else
			CCS9GPIOProcessInterface::byCache[this->dwChannel-1] &= ~(1 << this->pinNum);		
		
		status = CCS9GPIOProcessInterface::status_cache[this->dwChannel-1];
  	}	
  return true;
}

void CCS9GPIOProcessInterface::pollDI()
{
	//pinNum is init at -1 and would remain -1 until a valid value was passed
	if(-1 != this->pinNum)
  	{	
		//this->byDMsk = 1 << this->pinNum;
				
		//read the pin and store in a cache and will be retrieved by readPin		
		DWORD dwRet = EA_DIN_Read(this->RIdx , &(CCS9GPIOProcessInterface::byCache[this->dwChannel-1]), this->byDMsk);	
	
		if (dwRet)
		{	
			//TODO: probably stop polling and report error		
			return;
		}		
  	}
}

void CCS9GPIOProcessInterface::pollDO()
{
	//pinNum is init at -1 and would remain -1 until a valid value was passed
	if(-1 != this->pinNum)
	{ 					
		//cache will be updated by writePin
		DWORD dwRet = EA_DOUT_Write(this->RIdx, &CCS9GPIOProcessInterface::byCache[this->dwChannel-1], this->byDMsk);
		
		if (dwRet)
		{
			//TODO: probably stop polling
			return;
		} 
  	}
}
