#ifndef _CS9GPIOPROCESSINTERFACE_H_
#define _CS9GPIOPROCESSINTERFACE_H_

//forte includes
#include <funcbloc.h>
#include <resource.h>
#include <forte_bool.h>
#include <forte_string.h>
#include <stdint.h>

//eapi includes
#include "eapi_def.h"
#include "eapi_errors.h"
#include "eapi_types.h"
#include "eapi_pub.h"
#include "EAPI_UDEF.h"

class CCS9GPIOProcessInterface 
{
  public:
    CCS9GPIOProcessInterface();	
	virtual ~CCS9GPIOProcessInterface();
	enum EPinMode { enDI, enDO };
	
  	//initializes EAPI resources-- DIN if isInput is true, DOUT if isInput is false
    bool initialise(const CIEC_INT pin, const CIEC_INT channel, EPinMode isInput, CIEC_WSTRING &status);
	//releases EAPI resources
    bool deinitialise();
	//will read the value of the specified pin and channel from initialize
    bool readPin(CIEC_BOOL &in, CIEC_WSTRING &status);
	//will write the value at the specified pin and channel from initialize 
    bool writePin(const CIEC_BOOL &out, CIEC_WSTRING &status);	

  private:
  	//this class is used to create a static array for polling channel 1-4
	//channel 1 and 3 will be DO, rest will be DI
  	class ChannelPoller : public CThread
	{
		private:
			void run()
			{
				while(this->m_bAlive)
				{
					if (pBasePI->pinMode == enDI)
						pBasePI->pollDI();
					else
					{
						pBasePI->pollDO();
						//this->m_bAlive = false; //very tempting to this but this could lead to some QX not being able to start the write thread
					}
				}				
			}
		public:
			CCS9GPIOProcessInterface* pBasePI;
	};
	
	static ChannelPoller channelPoller[]; //the objects will be created by calling the default constructor  	 
	static CIEC_WSTRING  status_cache[]; 
	static U32  byCache[];
				
	int      pinNum;
	EPinMode pinMode;
	U32      dwChannel;	 
	U32      RIdx;
	U32*     pRIdx;	
	U32      byDMsk;	
	
	void pollDI();
	void pollDO();	    
};

#endif /* _CS9GPIOPROCESSINTERFACE_H_ */
