#include "processinterface.h"

CProcessInterface::CProcessInterface()
{
	//Do not forget to add the constructor code
	#error add constructor code
}

CProcessInterface::~CProcessInterface()
{
	//Do not forget to add the destructor code
	#error add destructor code
}

bool CProcessInterface::initialise(CIEC_STRING& status)
{
	//Do not forget to add initialize code here 
	#error add init code
}

bool CProcessInterface::deinitialise(CIEC_STRING &status)
{
	//Do not forget to add deinitialise code here
	#error add deinit code
}

bool CProcessInterface::write(const CIEC_BOOL out)
{
	//Do not forget to add write code 
	#error add write code
}

bool CProcessInterface::read(CIEC_BOOL &in)
{
	//Do not forget to add read code
	#error add read code
}

