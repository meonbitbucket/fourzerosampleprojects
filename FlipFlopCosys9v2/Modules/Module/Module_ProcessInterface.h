#ifndef _PROCESSINTERFACE_H_
#define _PROCESSINTERFACE_H_

#include <funcbloc.h>
#include <resource.h>
#include <forte_bool.h>
#include <forte_string.h>
#include <stdint.h>

class CProcessInterface 
{	
	public:
		CProcessInterface();
		~CProcessInterface();
			
		bool initialise(CIEC_STRING &status);	
		bool deinitialise(CIEC_STRING &status);
		bool write(const CIEC_BOOL out);
		bool read(CIEC_BOOL &in);
};

#endif /*_PROCESSINTERFACE_H_*/
