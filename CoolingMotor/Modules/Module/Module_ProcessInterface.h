#ifndef _PROCESSINTERFACE_H_
#define _PROCESSINTERFACE_H_

#include <funcbloc.h>
#include <resource.h>
#include <forte_bool.h>
#include <forte_string.h>
#include <stdint.h>

class CProcessInterface 
{	
	public:
		CProcessInterface();
		~CProcessInterface();

		enum ECheckPinResult
		{
			enInvalidPinNum,
			enPinInUse,
			enPinOk
		};

		enum EPinMode
		{
			enDI,
			enDO
		};	
	
		bool initialise(CIEC_STRING pin, EPinMode mode, CIEC_STRING &status);	
		bool deinitialise(CIEC_STRING pin, EPinMode mode, CIEC_STRING &status);
		bool write(const CIEC_BOOL out);
		bool read(CIEC_BOOL &in);
	
	private:
		int pinNum;
		ECheckPinResult checkPin(EPinMode pin_mode);
};

#endif /*_PROCESSINTERFACE_H_*/
