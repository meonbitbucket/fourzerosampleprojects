#include "module_processinterface.h"

CProcessInterface::CProcessInterface()
{
	#error add constructor code
}

CProcessInterface::~CProcessInterface()
{
	#error add destructor code
}

bool CProcessInterface::initialise(CIEC_STRING pin, EPinMode mode, CIEC_STRING& status)
{
	CIEC_INT param;
	param.fromString(pin.getValue());
	this->pinNum = param;
	#error add init code
}

bool CProcessInterface::deinitialise(CIEC_STRING pin, EPinMode pinMode, CIEC_STRING &status)
{
	#error add deinit code
}

bool CProcessInterface::write(const CIEC_BOOL out)
{
	#error add write code
}

bool CProcessInterface::read(CIEC_BOOL &in)
{
	#error add read code
}

CProcessInterface::ECheckPinResult CProcessInterface::checkPin(EPinMode pinMode)
{
	#error add check pin code
}


